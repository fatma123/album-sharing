<?php

use Illuminate\Database\Seeder;

class PermissionsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('permissions')->insert([
                [
                    'name' => 'groups',
                    'group_id' => 1
                ], [
                    'name' => 'home',
                    'group_id' => 1
                ], [
                    'name' => 'admins',
                    'group_id' => 1
                ], [
                    'name' => 'clients',
                    'group_id' => 1
                ], [
                    'name' => 'albums',
                    'group_id' => 1
                ],
            ]
        );
    }
}
