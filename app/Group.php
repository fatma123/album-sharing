<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Group extends Model
{
    protected $fillable=['name'];

    public function Permissions() {
        return $this->hasMany(Permission::class, 'group_id', 'id');
    }
}
