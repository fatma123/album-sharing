<?php

namespace App;

use App\Http\Traits\ImageOperations;
use Illuminate\Database\Eloquent\Model;

class UserAlbum extends Model
{
    use ImageOperations;
    protected $fillable=['user_id','file','status'];
}
