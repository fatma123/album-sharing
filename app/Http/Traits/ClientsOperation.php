<?php


namespace App\Http\Traits;

use App\User;
use Illuminate\Http\Request;

trait ClientsOperation
{
    public function StoreClient($request)
    {
        $inputs = $request->all();
        return User::create($inputs);
    }

    public function UpdateClient($client,$request)
    {
        $inputs = $request->all();
        $client->update($inputs);
        return $client;
    }

}