<?php


namespace App\Http\Traits;

use App\Banners;
use App\Books;
use App\MeetingImages;
use App\NewsImages;
use App\ProviderAlbum;
use App\UserAlbum;
use Illuminate\Http\Request;

trait ImageOperations
{
    public function getImageAttribute($value)
    {
        return getimg($value);
    }

    public function getFileAttribute($value)
    {
        return getimg($value);
    }

    public function setImageAttribute($value){

        $this->attributes['image'] = uploader($value);
    }


    public function Gallery($request,$user){
        if ($request->hasFile('files')) {
//            dd('hi');
            foreach ($request['files'] as $key => $item) {
                $imageName = $path = \Storage::disk('public')->putFile('photos', $item);
                $ads_image = new UserAlbum();
                $ads_image->user_id = $user->id;
                $ads_image->file = $imageName;
                $ads_image->status = $request->status;
                $ads_image->save();
            }
        }
    }




}
