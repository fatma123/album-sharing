<?php


namespace App\Http\Traits;


trait UsersOperations
{
    use ImageOperations;


    public function UpdateUser($user, $request)
    {
        $inputs = $request->all();
//        dd($inputs);
        $user->update($inputs);
        $this->Gallery($request, $user);

        return $user;
    }

}
