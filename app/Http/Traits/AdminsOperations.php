<?php


namespace App\Http\Traits;


use App\Awards;
use App\MeetingArticles;
use App\User;

trait AdminsOperations
{
    public function StoreAdmin($request)
    {
        $inputs = $request->all();
        return User::create($inputs);
    }

    public function UpdateAdmin($article, $request)
    {
        $inputs = $request->all();
        $article->update($inputs);
        return $article;
    }

}