<?php


namespace App\Http\Traits;

use App\Group;


trait GroupsOperations
{
    public function StoreGroup($request)
    {
        $inputs = $request->all();
        $group = Group::create($inputs);

        if (isset($request['permissions'])) {
            $permissions = $request['permissions'];
            foreach ($permissions as $item) {
                $group->Permissions()->create([
                    'name' => $item
                ]);

            }
        }
        return $group;
    }

    public function UpdateGroup($group, $request)
    {
        $inputs = $request->all();
        $group->update($inputs);

        if (isset($request['permissions'])) {
            $permissions = $request['permissions'];
            foreach ($permissions as $item) {
                $group->Permissions()->firstOrCreate([
                    'name' => $item
                ]);

                if (isset($request['can-permissions'])) {

                    $subpermission = $request['can-permissions'];

                    foreach ($subpermission as $item) {
                        $group->Permissions()->firstOrCreate([
                            'name' => $item
                        ]);
                    }
                }
            }
            $group->Permissions()->whereNotIn('name', $request->permissions)->delete();
        }
        return $group;
    }

}