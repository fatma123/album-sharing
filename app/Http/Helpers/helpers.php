<?php

use Intervention\Image\Facades\Image;

function api()
{
    return auth('api');
}

/**
 * Setting Name
 * @param $name
 * @return mixed
 */
function getsetting($name)
{
    $setting = \App\Settings::where('name', $name)->first();
    if (!$setting) return "";
    return $setting->ar_value;
}

/**
 * Upload Path
 * @return string
 */
function uploadpath()
{
    return 'photos';
}

function user()
{
    return auth()->user();
}

function roles($name = null){
    $arr = [
        'admin'=>'مشرف',
        'user'=>'مستخدم',
        'delivery'=>'توصيل'
    ];

    if ($name != null)
        return $arr[$name];

    return $arr;
}

/**
 * Get Image
 * @param $filename
 * @return string
 */
function getimg($filename)
{
    if (!empty($filename)) {
        $base_url = url('/');
        return $base_url . '/storage/' . $filename;
    } else {
        return '';
    }
}

function collectPagination($collection,$perPage)
{
    $request = request();
    $collection = collect($collection);
    $page = $request->has('page') ? $request->page : 1;
    $_collection = $collection->forPage($page,$perPage);
    $pagination['data'] = $_collection->all();
    $pagination['paginate'] = [
        'total' => count($collection),
        'count' => count($pagination['data']),
        'per_page' => $perPage,
        'next_page_url'=>count($collection->forPage($page,$perPage)->all()) == 0 ? null : $perPage+1,
        'prev_page_url'=> $page >1 ? $page-1 : null,
        'current_page' => $page,
        'total_pages' => ceil(count($collection)/$perPage)
    ];
    return collect($pagination);
}


function getLang($collection, $target)
{
    if (app()->getLocale() == 'en') {
        return $collection['en_' . $target];
    } else {
        return $collection['ar_' . $target];
    }
}

/**
 * Upload an image
 * @param $img
 */
function uploader($value)
{
    $path = \Storage::disk('public')->putFile(uploadpath(), $value);
    return $path;
}

function _uploader($request, $img_name)
{
    $path = \Storage::disk('public')->putFile(uploadpath(), $request->file($img_name));
    return $path;
}

function multipleUploader($request, $img_name, $model, $options = [])
{
    foreach ($request->$img_name as $key => $item) {
        if (in_array($item->extension(),['mp4','ogx','oga','ogv','ogg','webm'])){
            $photo = \Illuminate\Support\Facades\Storage::disk('public')->putFile('videos', $item);
            $options['type'] = 'video';
        }
        else{
            $photo = \Illuminate\Support\Facades\Storage::disk('public')->putFile(uploadpath(), $item);
            $options['type'] = 'image';
        }
        $items[$key] = $model->firstOrCreate([
            $img_name => $photo
        ], $options);
    }

    return $items;
}

function uploaderWithOptimize($request, $img_name, $model, $options = [])
{
    foreach ($request->$img_name as $key => $item) {
        if (in_array($item->extension(),['mp4','ogx','oga','ogv','ogg','webm'])){
            $path = \Illuminate\Support\Facades\Storage::disk('public')->putFile('videos', $item);
            $options['type'] = 'video';
        }
        else{
            $_image = Image::make($item);
            if (file_exists(getimg(setting('image_logo')))){
                $watermark = \Illuminate\Support\Facades\Storage::disk('public')->read(setting('image_logo'));
                $width = $_image->width() *70/100;
                $height = $_image->height() *70/100;
                $watermark = Image::make($watermark)->resize($width-$width*80/100,$height-$height*80/100);
                $_image->insert($watermark, 'bottom-right',10,10);

            }else{
                $width = $_image->width() *70/100;
                $height = $_image->height() *70/100;
            }
//            $watermark = Image::make(asset('storage/'.setting('image_logo')))->resize(50,15);
            $path = 'photos/' . \Illuminate\Support\Str::random(50).'.png';
            $_image->resize($width, $height)->save(public_path('storage').'/'.$path);
            $options['type'] = 'image';
        }
        $items[$key] = $model->firstOrCreate([
            $img_name => $path
        ], $options);
    }

    return $items;
}



function deleteImg($img_name)
{
    \Storage::disk('public')->delete(uploadpath(), $img_name);
    return True;
}
function deleteFile($file)
{
    $file = explode('/',$file);
    \Storage::disk('public')->delete($file[0], $file[1]);
    return True;
}

function percentage($first, $second){
    $pre=-$first + $second;
    if($first==0){
        $pre=$pre / 1 ;
    }else
    {$pre=$pre/ $first*100;}
    return $pre;
}

function AccountStatus( $status= null)
{
    $array = [
        '1' => 'مفعل',
        '0' => 'غير مفعل',
    ];
    return $array[$status];
}

function Status( $status= null)
{
    $array = [
        'active' => 'مفعل',
        'not_active' => 'غير مفعل',
    ];
    return $array[$status];
}
function OrderStatus( $status= null)
{
    $array = [
        'pending' => 'قيد الإنتظار',
        'accepted' => 'تم الموافقة',
        'refused' => 'تم الرفض',
        'finished' => 'منتهية',
        'cancel' => 'قام العميل بإلغاء الطلب '
    ];
    return $array[$status];
}
function TransActionStatus( $status= null)
{
    $array = [
        'accepted' => ' الموافقة',
        'not_accepted' => ' الرفض',
    ];
    return $array[$status];
}

function PaymentWay( $status= null)
{
    $array = [
        'cache' => 'عند تقديم الخدمة',
        'electronic_payment' => 'إلكتروني',
    ];
    return $array[$status];
}

function DeliverWay($status=null){

    $array = [
        'home'=>'للمنزل',
        'consultation'=>'استشارة'
    ];
    return $array[$status];
}

function Type( $type= null)
{
    $array = [
        'visa' => 'فيزا',
        'master' => 'ماستر',
        'mada' => 'مدى',
    ];
    return $array[$type];
}

function MultiMediaType()
{
    $array = [
        'image'=>'صورة',
        'video'=>'فيديو',
    ];
    return $array;
}


function permissions()
{
    $countries = App\Permission::all()->mapWithKeys(function ($item) {
        return [$item['id'] => $item['title']];
    });
    return $countries;
}

function users()
{
    $users = App\User::all()->mapWithKeys(function ($item) {
        return [$item['id'] => $item['name']];
    });
    return $users;
}



function groups()
{
    $users = App\Group::all()->mapWithKeys(function ($item) {
        return [$item['id'] => $item['name']];
    });
    return $users;
}


function setting($name)
{
    return getLang(\App\Settings::where('name', $name)->first(),'value');
}

function services()
{
    $subscribers = App\Service::all()->mapWithKeys(function ($item) {
        return [$item['id'] => $item['ar_name']];
    });
    return $subscribers;
}

function banks()
{
    $banks = App\Bank::all()->mapWithKeys(function ($item) {
        return [$item['id'] => $item['ar_name']];
    });
    return $banks;
}

function ChangeOrderStatus()
{
    $array = [
        'pending' => 'قيد الإنتظار',
        'accepted' => 'تم الموافقة',
        'refused' => 'تم الرفض',
        'finished' => 'منتهية',
        'cancel' => 'قام العميل بإلغاء الطلب '
    ];
    return $array;

}
function ChangeTransStatus()
{
    $array = [
        'accepted' => ' الموافقة',
        'not_accepted' => ' الرفض'
    ];
    return $array;

}



function popup($name)
{
    $ms = 5000;
    $types = ['success', 'warning', 'info', 'error', 'basic'];
    $crud = ['add', 'update', 'delete'];

    if (in_array($name, $crud)) {
        if ($name == 'add') {
            return alert()->success('تم الاضافه بنجاح')->autoclose($ms);
        } elseif ($name == 'update') {
            return alert()->success('تم التعديل بنجاح')->autoclose($ms);
        } elseif ($name == 'delete') {
            return alert()->success('تم الحذف بنجاح')->autoclose($ms);
        }
    }

    if (in_array(array_keys($name)[0], $types)) {
        $alert = array_keys($name)[0];
        return alert()->$alert(array_values($name)[0])->autoclose($ms);
    }


    if (array_keys($name)[0] == 'rules') {

        $validator = \Illuminate\Support\Facades\Validator::make(request()->all(), array_values($name)[0]);

        if ($validator->fails()) {
            alert()->error($validator->errors()->first())->autoclose($ms);
            return true;
        }
        return false;

    }


    function fix_null($item)
    {
        return blank($item) ? 0 : $item;
    }

    function fix_null_string($item)
    {
        return blank($item) ? '' : $item;
    }

    function fix_null_array($item)
    {
        return blank($item) ? [] : $item;
    }

    function fix_null_object($item)
    {
        return blank($item) ? json_encode() : $item;
    }


}




function checkimag($filename)
{
    if ($filename != null)
        return file_exists(public_path('storage/') . $filename);
    else
        return false;
}

function convert($words)
{
    $words = explode(' ', $words);
    $_word = '';

    if (count($words) > 1) {
        if (app()->getLocale() == 'ar') {
            $convert = $words;
            if (in_array('name', $convert) || in_array('description', $convert) || in_array('model', $convert)) {
                if (in_array('name', $convert))
                    $key = array_search('name', $convert);
                else if (in_array('description', $convert))
                    $key = array_search('description', $convert);
                else if (in_array('model', $convert))
                    $key = array_search('model', $convert);


                foreach ($convert as $k => $value) {
                    if ($convert[0] != 'name' || $convert[0] != 'description' || $convert[0] != 'model') {
                        if ($k == $key) {
                            $_word .= trans('lang.' . $convert[$key - 1]) . ' ';
                        } else if ($k == $key - 1) {
                            $_word .= trans('lang.' . $convert[$key]) . ' ';
                        } else {
                            $_word .= trans('lang.' . $value) . ' ';
                        }
                    } else {
                        foreach ($words as $word) {
                            $_word .= trans('lang.' . $word) . ' ';

                        }
                    }
                }
            } else {
                foreach ($words as $word) {
                    $_word .= trans('lang.' . $word) . ' ';

                }
            }

        } else {
            foreach ($words as $word) {
                $_word .= trans('lang.' . $word) . ' ';

            }
        }
    } else {
        foreach ($words as $word) {
            $_word .= trans('lang.' . $word) . ' ';

        }
    }

    return $_word;
}

function fcm_server_key()
{
    return 'AAAARoSk8_U:APA91bHfFCNs5G7IBA9bV8uIdnVbKRS1LqSbGppHi4K58gToMiHs8jijM13N86zn1op3S0YWLstZpfktQ7L8R6HQr1hyLDHIJ21Wi1cRH-FeOhlwIiKMNbpzTZYAqNoqsHASlcISUcFh';
}
function notifyByFirebase($title, $body, $tokens, $data = [], $is_notification = true)
{
    $registrationIDs = $tokens;
// prep the bundle
// to see all the options for FCM to/notification payload:
// https://firebase.google.com/docs/cloud-messaging/http-server-ref#notification-payload-support
// 'vibrate' available in GCM, but not in FCM
    $fcmMsg = array(
        'body' => $body,
        'title' => $title,
        'sound' => "default",
        'color' => "#203E78"
    );
// I haven't figured 'color' out yet.
// On one phone 'color' was the background color behind the actual app icon.  (ie Samsung Galaxy S5)
// On another phone, it was the color of the app icon. (ie: LG K20 Plush)
// 'to' => $singleID ;      // expecting a single ID
// 'registration_ids' => $registrationIDs ;     // expects an array of ids
// 'priority' => 'high' ; // options are normal and high, if not set, defaults to high.
    $fcmFields = array(
        'registration_ids' => $registrationIDs,
        'priority' => 'high',
        'data' => $data
    );
    if ($is_notification) {
        $fcmFields['notification'] = $fcmMsg;
    }
    $headers = array(
        'Authorization: key=' . fcm_server_key(),
        'Content-Type: application/json'
    );
    /*        info("API_ACCESS_KEY_client: ".env('API_ACCESS_KEY_client'));
            info("PUSHER_APP_ID: ".env('PUSHER_APP_ID'));*/
    $ch = curl_init();
    curl_setopt($ch, CURLOPT_URL, 'https://fcm.googleapis.com/fcm/send');
    curl_setopt($ch, CURLOPT_POST, true);
    curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
    curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
    curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($fcmFields));
    $result = curl_exec($ch);
    curl_close($ch);
    info($result);
    return $result;
}
