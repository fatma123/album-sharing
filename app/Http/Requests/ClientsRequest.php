<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Support\Facades\Auth;

class ClientsRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $rules = [
            'name'=>'required|string|max:191|unique:users,name,'.request()->id,
            'email'=>'required|email|max:255|unique:users,email,'. request()->id,
            'file'=>'sometimes|image|mimes:jpeg,png,jpg,gif,svg|max:2048',
            'status'=>'sometimes',
            'old_password' => 'required',
            'password' => 'required|min:6|confirmed'
        ];
        if ($this->getMethod() == 'PATCH') {
            $rules = [
                'name'=>'required|string|max:191|unique:users,name,'.request()->id,
                'email'=>'required|email|max:255|unique:users,email,'. request()->id,
                'password'=>'required|min:6|confirmed',
                'file'=>'sometimes',
                'status'=>'sometimes'
            ];
        }
        return $rules;
    }
}
