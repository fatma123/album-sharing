<?php

namespace App\Http\Controllers\Website;

use App\Http\Controllers\Controller;
use App\Http\Requests\ClientsRequest;
use App\Http\Traits\UsersOperations;
use App\UserAlbum;
use Hash;

class IndexController extends Controller
{
    use UsersOperations;

    public function Albums()
    {
        if (auth()->check()) {
            $albums = UserAlbum::where('user_id', auth()->user()->id)->get();
        } else {
            $albums = UserAlbum::whereStatus('public')->get();
        }
        return view('website.albums', ['items' => $albums]);
    }


    public function Profile()
    {
        return view('website.profile');
    }


    public function EditProfile()
    {
        $images = UserAlbum::where('user_id', auth()->user()->id)->get();
        return view('website.edit_profile', ['images' => $images]);
    }

    public function UpdateProfile(ClientsRequest $request)
    {
        $user = auth()->user();

        if (Hash::check($request->old_password, auth()->user()->password)) {
//            dd('hi');
            $this->UpdateUser($user, $request);
        } else {
            alert()->error('كلمة المرور القديمة غير صحيحة !')->autoclose(5000);
            return back();
        }
        popup('update');
        return back();
    }
}
