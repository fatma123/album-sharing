<?php

namespace App\Http\Controllers\Website;

use App\UserAlbum;
use App\Http\Controllers\Controller;

class AjaxOperationsController extends Controller
{



    public function UserAlbumDelete($id)
    {
        $image = UserAlbum::find($id);
        if ($image) {
            $image->delete();
            return "image deleted";
        } else {
            return "not found";
        }


    }

}
