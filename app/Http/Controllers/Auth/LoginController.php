<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Http\Request;

class LoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    use AuthenticatesUsers;

    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    protected $redirectTo = '/';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest')->except('logout');
    }

    public function showLoginForm()
    {
        return view('website.auth.login');
    }

    public function logout(\Request $request)
    {
        auth()->logout();
        return redirect('/');
    }

    public function login(Request $request)
    {
        $validator = \Illuminate\Support\Facades\Validator::make(request()->all(), [

            'email' => ['required'],
            'password' => ['required'],

        ]);

        if ($validator->fails()) {
            return response()->json(['response' => $validator->errors()->first()]);

        }

        $validator = \Validator::make($request->all(), [
            'email' => 'required|email',
            'password' => 'required',
        ]);

        if ($validator->passes()) {
            if (auth()->attempt(array('email' => $request->input('email'),
                'password' => $request->input('password')),true))
            {
                return response()->json('success');
            }
            return response()->json([
                'error' => [
                    'email' => 'Sorry User not found.'
                ]
            ]);
        }

        return response()->json(['error'=>$validator->errors()->all()]);
    }
}
