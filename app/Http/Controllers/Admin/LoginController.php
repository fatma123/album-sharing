<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class LoginController extends Controller
{

    public function showLogin()
    {

        if (Auth::check()) {
            return redirect('/dashboard');
        }
        return view('admin.auth.login');

    }

    public function Login(Request $request)
    {
        $this->validate($request, [
            'email' => 'required|email',
            'password' => 'required'
        ]);

        if (Auth::attempt(['email' => $request->email, 'password' => $request->password])) {

            if (Auth::User()->role != "admin") {
//                dd(Auth::User()->is_active);
                return  redirect('/login');
            }
            else {
                return redirect('/dashboard');
            }

        }else{
            return redirect('/');
        }

    }
}
