<?php

namespace App\Http\Middleware;

use App\Permission;
use Closure;

use Illuminate\Support\Facades\Auth;

class CheckRole
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next,$role)
    {
        $found = Permission::where('group_id', Auth::User()->group_id)->where('name', $role)->count();
        if ($found == 0) {
            alert()->success('لا تملك هذه الصلاحية !')->autoclose(5000);
            return redirect('dashboard');
        }
        return $next($request);
    }
}
