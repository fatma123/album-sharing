@extends('website.layout.master')

@section('title')
    EditProfile
@stop

@section('headers')
    <style>
        #clickableAwesomeFont {
            cursor: pointer
        }
    </style>
    @endsection

@section('content')


    <section class="profile bkg">
        <div class="container">

            @include('admin.common.alert')
            @include('admin.common.errors')
            <form class="form2 width-80 form-no-hover" action="{{route('website.UpdateProfile')}}" method = "POST"  enctype="multipart/form-data">

                @csrf

                <div class="row">


                    <div class="col-12">
                        <div class="form-group">
                            <label> اسم المستخدم </label>
                            <input name="name" type="text"  id="name" class="form-control" value="{{auth()->user()->name}}">
                            <span class="focus-border"></span>
                        </div>
                    </div>
                    <div class="col-12">
                        <div class="form-group">
                            <label> البريد الإلكترونى </label>
                            <input type="email" name="email"  id="email" class="form-control" value="{{auth()->user()->email}}">
                            <span class="focus-border"></span>
                        </div>
                    </div>

                    <div class="col-12">
                        <div class="form-group">
                            <label> Old Password </label>
                            <input type="password" class="form-control" id="password" name="old_password">
                            <span class="focus-border"></span>
                        </div>
                    </div>
                    <div class="col-12">
                        <div class="form-group">
                            <label>New Password </label>
                            <input type="password" class="form-control" id="pwd" name="password">
                            <span class="focus-border"></span>
                        </div>
                    </div>
                    <div class="col-12">
                        <div class="form-group">
                            <label> password Confirmation </label>
                            <input type="password" class="form-control" id="pwd" name="password_confirmation">
                            <span class="focus-border"></span>
                        </div>
                    </div>


                    @if (isset($images))
                        <div class="col-12">
                            <div class="form-group form-float">
                                <label class="form-label">الصور الحالية :</label>
                                <div class="form-line row">
                                    @foreach($images as $image)
                                        <div class="col-sm-3">
                                            <div class="flex-img">
                                                <img class="img-preview" src="{{$image->file}}"
                                                     style="height: 50px; width: 50px">
                                                <div class="row ">
                                                    {{--                            @dd($image->id)--}}
                                                    <span onclick="goDoSomething(this)" data-id="{{$image->id}}" id='clickableAwesomeFont'
                                                          class=" delete btn btn-danger"><i class="fas fa-trash"></i></span>
                                                </div>
                                            </div>
                                        </div>
                                    @endforeach
                                </div>
                            </div>
                        </div>
                    @endif


                    <div class="col-12">
                        <label class="form-label">اضافة صور للألبوم</label>
                        <div class="form-line">
                            {!!Form::file('files[]', ['class'=>'form-control','multiple'=>'yes','id'=>'file'])!!}
                        </div>
                    </div>

                    <br><hr><hr>
                    <div class="col-12">
                        <div class="col-12">
                            <label class="form-label">نوع الألبوم </label>
                            {!! Form::radio("status",'private',null,['class'=>'with-gap','id'=>'radio_3']) !!}
                            <label for="radio_3"> خاصة </label>

                            {!! Form::radio("status",'public',null,['class'=>'with-gap','id'=>'radio_4']) !!}
                            <label for="radio_4">عامة</label>
                        </div>
                    </div>
                    {!! Form::input('hidden','id',auth()->user()->id,['class'=>'form-control']) !!}
                    <div class="text-center p-2">
                        <button type="submit" class="btn btn-gradiant">
                            Save
                        </button>
                    </div>

                </div>

            </form>

        </div>
    </section>

@stop

@push('scripts')
    <script>

        {{--$("#update").on("click", function (e) {--}}

            {{--// alert('hi');--}}
            {{--e.preventDefault();--}}
            {{--$.post('/update', {--}}
                {{--_token:"{{csrf_token()}}",--}}
                {{--name: $("#name").val(),--}}
                {{--email: $("#email").val(),--}}
                {{--password: $("#password").val(),--}}
                {{--password_confirmation: $("#password_confirmation").val(),--}}
                {{--file: $("#file").val()--}}
            {{--}, function(data) {--}}
                {{--if (data.response == '/'){--}}
                    {{--window.location.href = '/profile-edit';--}}
                {{--} else{--}}
                    {{--alert(data.response);--}}
                {{--}--}}
            {{--});--}}
        {{--});--}}

        function goDoSomething(identifier) {
            $.ajax({
                url: "{{asset('/album/delete')}}" + "/" + $(identifier).data('id'),
                success: function (result) {
                    window.location.reload();
                }
            });

        }
    </script>
@endpush