@extends('website.auth.layout.master')

@section('title')
    Register
@stop

@section('content')

    <section class="contact-us bg-light">
        <div class="container">
            <h3 class="text-center">Sign Up To Join Us</h3>

            <div class="row justify-content-center">
                <div class="col-md-7 col-sm-10">
                    <div class="contact-form">
                        @include('admin.common.errors')
                        @include('admin.common.alert')
                        <form>
                            @csrf
                            <div class="form-group ">
                                <label for="inputName">Write Your Name</label>
                                <input type="text" id="name" name="name" class="form-control"
                                       placeholder="Write Your Name">
                            </div>
                            <div class="form-group">
                                <label for="inputEmail">Your Email Addrss</label>
                                <input type="email" name="email" id="email" class="form-control"
                                       placeholder="Write Your Email">
                            </div>
                            <div class="form-group">
                                <label for="inputPassword">Enter Password </label>
                                <input type="password" name="password" id="password" class="form-control"
                                       placeholder=" Write Your password">
                            </div>

                            <div class="form-group">
                                <label for="inputConfirmPassword">Confirm Password </label>
                                <input type="password" name="password_confirmation" id="password_confirmation"
                                       class="form-control"
                                       placeholder="  Confirm Your password">
                            </div>

                            <input type="text" name="role"  id= "role" class="form-control" value="user" hidden>

                            <div class="text-center p-2">
                                <button type="submit" id="register" class="btn btn-gradiant">
                                    Sign Up
                                </button>
                            </div>

                            <div>
                                <b> <span>Have An Account ?</span> <a href="/login" class="main-color ">Login</a></b>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </section>

@stop

@push('scripts')
    <script>
        $("#register").on("click", function (e) {

            // alert('hi');
            e.preventDefault();
            $.post('register', {
                _token:"{{csrf_token()}}",
                name: $("#name").val(),
                email: $("#email").val(),
                password: $("#password").val(),
                password_confirmation: $("#password_confirmation").val(),
                role: $("#role").val()
            }, function(data) {
                if (data.response == '/'){
                    window.location.href = '/';
                } else{
                    alert(data.response);
                }
            });
        });



    </script>

@endpush
