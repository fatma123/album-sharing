@extends('website.auth.layout.master')

@section('title')
    login
@stop

@section('content')

<section class="contact-us bg-light">
    <div class="container">
        <h3 class="text-center">Login To Join Us</h3>

        <div class="row justify-content-center">
            <div class="col-md-7 col-sm-10">
                <div class="contact-form">
                    <form>
                        @csrf
                        <div class="form-group">
                            <label for="inputEmail">Your Email Addrss</label>
                            <input type="email" name="email" id="email" class="form-control"
                                   placeholder="Write Your Email">
                        </div>
                        <div class="form-group">
                            <label for="inputPassword">Your Password </label>
                            <input type="password"  name="password" id="password" class="form-control" placeholder=" Write Your password">
                        </div>


                        <div class="text-center p-2">
                            <button type="submit"  id="login" class="btn btn-gradiant">
                                login
                            </button>
                        </div>

                        <div >
                            <b> <span>Don't Have An Account ?</span> <a href="/register" class="main-color ">Sign Up</a></b>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</section>

    @stop

@push('scripts')
   <script>

       $("#login").on("click", function(e) {
           e.preventDefault();
           $.post('login', {
               _token:"{{csrf_token()}}",
               email: $("#login_email").val(),
               password:$("#login_password").val(),
           }, function(data) {
               if ($.isEmptyObject(data.error)) {
                   window.location.href = '/dashboard';
               } else {
                   swal('البيانات غير صحيحة');
               }
           });

       });

   </script>

    @endpush
