<link rel="stylesheet" type="text/css" media="screen" href="{{asset('website/css/bootstrap.min.css')}}">
<link rel="stylesheet" type="text/css" media="screen" href="{{asset('website/css/fontawesome.min.css')}}">
<link rel="stylesheet" type="text/css" media="screen" href="{{asset('website/css/animate.css')}}">
<link rel="stylesheet" type="text/css" media="screen" href="{{asset('website/css/all.min.css')}}">
<link rel="stylesheet" type="text/css" media="screen" href="{{asset('website/css/style.css')}}">
<link rel="stylesheet" type="text/css" media="screen" href="{{asset('website/css/responsive.css')}}">
