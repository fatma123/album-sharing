<div class="top_nav">
    <div class="container">
        <div class="row">
            <div class="col-sm-8">
                <ul class="d-flex about-site">
                    <li><a href="{{route('website.Album')}}">Album</a></li>

                    @auth
                    <form id="logout-form" action="{{route('logout')}}" method="POST" style="display: none;">
                        {{ csrf_field() }}
                    </form>
                    <li><a class="ar" onclick="event.preventDefault();
                            document.getElementById('logout-form').submit();" href="{{url('/logout')}}">Logout</a></li>
                        @else
                        <li><a href="/login">Login</a></li>
                        <li><a href="/register">Register</a></li>
                        @endauth
                </ul>
            </div>
        </div>
    </div>
</div>
<div class="logo text-center">
    <a class="navbar-brand" href="{{route('website.Album')}}index.html"><img src="{{asset('website/images/logo-m.png')}}" data-src="{{asset('website/images/logo-m.png')}}"
                                                   class="lazyload"></a>
</div>