<!DOCTYPE html>
<html>

<head>
    <title>Album Sharing- @yield('title')</title>
    <meta charset="UTF-8"/>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <meta name="author" content="RoQaY">
    <meta name="robots" content="index, follow">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content=" Smart Movies website">
    <meta name="keywords" content=" Smart Movies ">
    <meta name="csrf-token" content="V2G8zLS7dL5HzdfwxaBDewvJvAKCyeThQE4NBtJv">
    <meta content='width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0' name='viewport'/>
    @include('website.auth.layout.style')
    @yield('headers')
</head>

<body>

<div class="body_wrapper">
    <div class="preloader">
        <div class="preloader-loading">
            <img src="{{asset('website/images/logo-m.png')}}" data-src="images/logo-m.png" class="lazyload">
        </div>
    </div>

    @include('website.auth.layout.nav')
    @yield('content')

    @include('website.auth.layout.footer')


</div>
<!-- scripts -->
@include('website.auth.layout.scripts')

@yield('footer')
@stack('scripts')
@include('sweet::alert')
</body>

</html>