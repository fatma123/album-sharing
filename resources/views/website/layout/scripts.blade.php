<script src="{{asset('website/js/jquery-3.3.1.min.js')}}"></script>
<script src="{{asset('website/js/bootstrap.min.js')}}"></script>
<script src="{{asset('website/js/popper.min.js')}}"></script>
<script src="{{asset('website/js/lazysizes.min.js')}}"></script>
<script src="{{asset('website/js/fontawesome.min.js')}}"></script>
<script src="{{asset('website/js/all.min.js')}}"></script>
<script src="{{asset('website/js/wow.min.js')}}"></script>
<script src="{{asset('website/js/main.js')}}"></script>
<script src="{{asset('admin/plugins/sweetalert/sweetalert.min.js')}}"></script>
@include('sweet::alert')
<script>
$(document).on('click','#album', function(){
$.ajax({
    url: rootPath + '/',
    type: "GET", // not POST, laravel won't allow it
    success: function(data){
      $data = $(data); // the HTML content your controller has produced
      $('#container').fadeOut().html($data).fadeIn();
      }
  });
});


</script>
