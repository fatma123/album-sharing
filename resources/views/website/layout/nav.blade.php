<div class="top_nav">
    <div class="container">
        <div class="row">
            <div class="col-sm-8">
                <ul class="d-flex about-site">
                    <li id="album"><a href="{{route('website.Album')}}">Album</a></li>

                    @auth
                        <form id="logout-form" action="{{route('logout')}}" method="POST" style="display: none;">
                            {{ csrf_field() }}
                        </form>
                        <li><a class="ar" onclick="event.preventDefault();
                            document.getElementById('logout-form').submit();" href="{{url('/logout')}}">Logout</a></li>
                    @else
                        <li><a href="/login">Login</a></li>
                        <li><a href="/register">Register</a></li>
                    @endauth
                </ul>
            </div>

        </div>
    </div>
</div>
<nav class="navbar navbar-expand-lg navbar-light ">
    <div class="container">
        <a class="navbar-brand" href="{{route('website.Album')}}"><img src="{{asset('website/images/logo-m.png')}}" data-src="{{asset('website/images/logo-m.png')}}"
                                                       class="lazyload"></a>
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNav"
                aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
                    <span class="navbar-toggler-icon">
                        <ul class="menu-bars">
                            <li><span></span></li>
                            <li><span></span></li>
                            <li><span></span></li>
                        </ul>
                    </span>
        </button>
        <div class="collapse navbar-collapse" id="navbarNav">
            <ul class="navbar-nav ml-auto">
                <li class="nav-item">
                    <a class="nav-link" href="{{route('website.Album')}}">Album <span class="sr-only">(current)</span></a>
                </li>
                @auth
                <li class="nav-item">
                    <a class="nav-link" href="{{route('website.profile')}}">Profile</a>
                </li>

                    <li class="nav-item">
                        <a class="nav-link" href="{{route('website.EditProfile')}}">Edit Profile</a>
                    </li>

                @if(auth()->user()->role = "admin")
                <li class="nav-item">
                    <a class="nav-link" href="{{route('admin.home')}}"> DashBoard</a>
                </li>
                    @endif

                    @endauth
            </ul>
        </div>
    </div>
</nav>