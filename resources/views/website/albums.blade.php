@extends('website.layout.master')

@section('title')
    Albums
@stop


@section('content')

    <section class="check_demo_movie">
        <div class="container">
            <h2 class=" wow fadeInDown">Album <span class="main-color"> Sharing</span></h2>
            <div class="row">
                @forelse($items as $item)

                    <div class="col-md-4">
                        <div class="card wow fadeInUp" data-wow-duration="0.5s" data-wow-delay="0.5s">
                            <div class="card-header">
                                <img src="{{$item->file}}" class="lazyload">
                            </div>
                        </div>
                    </div>
                @empty
                    Sorry, There are No Albums Now..
                @endforelse

            </div>
        </div>
    </section>

@stop

