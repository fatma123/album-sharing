@extends('website.layout.master')

@section('title')
    Profile
@stop


@section('content')

    <section class="profile bkg">
        <div class="container">

            <form class="form2 width-80 form-no-hover">

                <div class="input-group-text">

                </div>
                <div class="col-12">
                    <div class="form-group">
                        <label> اسم المستخدم </label>
                        <input type="text" class="form-control" readonly value="{{auth()->user()->name}}">
                        <span class="focus-border"></span>
                    </div>
                </div>

                <div class="col-12">
                    <div class="form-group">
                        <label> البريد الإلكترونى </label>
                        <input type="email" class="form-control" readonly value="{{auth()->user()->email}}">
                        <span class="focus-border"></span>
                    </div>
                </div>

                <section class="check_demo_movie">
                    <div class="container">
                        <h2 class=" wow fadeInDown">Albums <span class="main-color"> Sharing</span></h2>
                        <p>User Albums</p>
                        <div class="row">

                            @forelse(auth()->user()->Album as $item)
                                <div class="col-md-4">
                                    <div class="card wow fadeInUp" data-wow-duration="0.5s" data-wow-delay="0.5s">
                                        <div class="card-header">
                                            <img src="{{$item->file}}" class="lazyload">
                                        </div>
                                        <div class="card-body">
                                            <p class="package-price">
                                                type : {{$item->status}}
                                            </p>
                                        </div>
                                    </div>
                                </div>
                            @empty
                             Their are No Albums Tell Now
                            @endforelse


                        </div>
                    </div>
                    <a href="{{route('website.EditProfile')}}">
                    <button class="btn btn-gradiant m-0">
                        Edit Profile
                    </button>
                    </a>
                </section>

            </form>
        </div>


    </section>

@stop