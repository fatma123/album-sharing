@extends('admin.layout.app')

@section('title')
    جميع المستخدمين
@endsection
@section('header')
    @include('admin.datatable.headers')
@endsection
@section('content')
    @include('admin.common.alert')
    <!-- Exportable Table -->
    <div class="row clearfix">
        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
            <div class="card">
                <div class="header">
                    <h2>
                        جميع الصور الخاصة ب {{$item->name}}
                    </h2>
                </div>
                <div class="body">
                    <table class="table table-bordered table-striped table-hover dataTable js-exportable">
                        <thead>
                        <tr>
                            <th>#</th>
                            <th>الصورة</th>
                            <th>الحالة</th>
                            <th>حذف الصورة</th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach($item->Album as $key=>$album)
                            <tr>
                                <td>{{++$key}}</td>
                                <td>
                                    <img src="{{$album->file}}" style="height: 50px; width: 50px">
                                </td>
                                <td>{{$album->status}}</td>
                                <td>

                                    <a onclick="goDoSomething(this)" data-id="{{$album->id}}"
                                       class=" delete btn btn-danger"><i
                                                class="fa fa-trash-o"></i></a>


                                </td>


                            </tr>
                        @endforeach

                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
    <!-- #END# Exportable Table -->

@endsection

@section('footer')
    @include('admin.datatable.scripts')
    <script>


        function goDoSomething(identifier) {

            // alert('hi');
            $.ajax({
                url: "{{asset('/album/delete')}}" + "/" + $(identifier).data('id'),
                success: function (result) {
                    window.location.reload();
                }
            });

        }
    </script>
@endsection
