@extends('admin.layout.app')

@section('title')
    تعديل العميل
    {{ $item->name }}
@endsection
@section('content')

    <!-- Basic Validation -->
    <div class="row clearfix">
        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
            <div class="card">
                <div class="header">
                    <h2>تعديل العميل {{ $item->name }}</h2>
                    <ul class="header-dropdown m-r--5">
                        <a href="{{route('admin.clients.index')}}">
                            <button class="btn btn-danger">كل العملاء</button>
                        </a>
                    </ul>
                </div>
                <div class="body">
                    {!!Form::model($item , ['route' => ['admin.clients.update' , $item->id] , 'method' => 'PATCH','enctype'=>"multipart/form-data",'files' => true]) !!}
                    @include('admin.clients.form')
                    {!!Form::close() !!}
                </div>
            </div>
        </div>
    </div>


    <!-- #END# Basic Validation -->
@endsection
