@include('admin.common.alert')
@include('admin.common.errors')


<div class="form-group form-float">
  <label class="form-label">الإسم</label>
  <div class="form-line">
    {!! Form::text("name",null,['class'=>'form-control','placeholder'=>'  الاسم  '])!!}
  </div>
</div>

<div class="form-group form-float">
  <label class="form-label">الإيميل</label>
  {!! Form::email("email",null,['class'=>'form-control','placeholder'=>'  الايميل '])!!}
  <div class="form-line">
  </div>
</div>


<div class="form-group form-float">
    <label >الجوال </label>
    {!! Form::text("phone",null,['class'=>'form-control','placeholder'=>'  الجوال  '])!!}
    <div class="form-line">
    </div>
</div>

<div class="form-group form-float">
  <label class="form-label">كلمة المرور</label>
    {!! Form::input('password','password',null,['class'=>'form-control','placeholder'=>'كلمة المرور']) !!}
    <div class="form-line">
  </div>
</div>

<div class="form-group form-float">
  <label class="form-label">تكرار كلمة المرور</label>
    {!! Form::input('password','password_confirmation',null,['class'=>'form-control','placeholder'=>'تكرار كلمة المرور']) !!}

    <div class="form-line">
  </div>
</div>


    <div class="form-group">
        {!! Form::radio("is_active",1,null,['class'=>'with-gap','id'=>'radio_3']) !!}
        <label for="radio_3"> تفعيل </label>

        {!! Form::radio("is_active",0,null,['class'=>'with-gap','id'=>'radio_4']) !!}
        <label for="radio_4" >عدم تفعيل</label>
    </div>

{!! Form::input('hidden','role','client',['class'=>'form-control']) !!}

@if(isset($item->id))
{!! Form::input('hidden','id',$item->id,['class'=>'form-control']) !!}
@endif
<button class="btn btn-primary waves-effect" type="submit">حفظ</button>
