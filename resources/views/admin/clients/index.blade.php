@extends('admin.layout.app')

@section('title')
    جميع المستخدمين
@endsection
@section('header')
    @include('admin.datatable.headers')
@endsection
@section('content')
    @include('admin.common.alert')
    <!-- Exportable Table -->
    <div class="row clearfix">
        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
            <div class="card">
                <div class="header">
                    <h2>
                        جميع المستخدمين
                    </h2>
                </div>
                <div class="body">
                    <table class="table table-bordered table-striped table-hover dataTable js-exportable">
                        <thead>
                        <tr>
                            <th>#</th>
                            <th>الاسم</th>
                            <th>الايميل</th>
                            <th>حذف المستخدم</th>
                            <th>مشاهدة الألبوم</th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach($items as $key=>$item)
                            <tr>
                                <td>{{++$key}}</td>
                                <td>{{$item->name}}</td>
                                <td>{{$item->email}}</td>
                                <td>

                                    <a onclick="goDoSomething(this)" data-id="{{$item->id}}"
                                       class=" delete btn btn-danger"><i
                                                class="fa fa-trash-o"></i></a>


                                </td>
                                <td>

                                    <a href="{{route('admin.clients.show',['id'=>$item->id])}}"
                                       class="btn btn-info btn-circle">
                                        <i style="padding-top:5px;padding-left: 6px;"
                                           class="fa fa-eye"></i>
                                    </a>
                                </td>


                            </tr>
                        @endforeach

                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
    <!-- #END# Exportable Table -->

@endsection

@section('footer')
    @include('admin.datatable.scripts')
    <script>


        function goDoSomething(identifier) {

            alert('hi');
            $.ajax({
                url: "{{asset('/dashboard/clients/delete')}}" + "/" + $(identifier).data('id'),
                success: function (result) {
                    window.location.reload();
                }
            });

        }
    </script>
@endsection
