@extends('admin.layout.app')

@section('title')
    الصفحه الرئيسه
@endsection

@section('content')
    <!-- Widgets -->
    <div class="row clearfix">

        <a href="{{route('admin.admins.index')}}" style="color:#eee">
        <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
            <div class="info-box bg-lime hover-expand-effect">
                <div class="icon">
                    <i class="material-icons">assignment_ind</i>
                </div>

                <div class="content">
                    <div class="text">
                      عدد أعضاء إدارة النظام
                    </div>
                    <div class="number count">{{\App\User::where('role','admin')->count()}}</div>
                </div>

            </div>
        </div>
        </a>





        <a href="{{route('admin.clients.index')}}" style="color:#eee">
            <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
                <div class="info-box bg-lime hover-expand-effect">
                    <div class="icon">
                        <i class="material-icons">assignment_ind</i>
                    </div>

                    <div class="content">
                        <div class="text">
                            عدد المستخدمين
                        </div>
                        <div class="number count">{{\App\User::where('role','user')->count()}}</div>
                    </div>

                </div>
            </div>
        </a>

        <a href="{{route('admin.groups.index')}}" style="color:#eee">
            <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
                <div class="info-box bg-lime hover-expand-effect">
                    <div class="icon">
                        <i class="material-icons">assignment_ind</i>
                    </div>

                    <div class="content">
                        <div class="text">
                             عدد المجموعات
                        </div>
                        <div class="number count">{{\App\Group::count()}}</div>
                    </div>

                </div>
            </div>
        </a>


        <a href="{{route('admin.groups.index')}}" style="color:#eee">
            <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
                <div class="info-box bg-lime hover-expand-effect">
                    <div class="icon">
                        <i class="material-icons">assignment_ind</i>
                    </div>

                    <div class="content">
                        <div class="text">
                            عدد الألبومات
                        </div>
                        <div class="number count">{{\App\UserAlbum::count()}}</div>
                    </div>

                </div>
            </div>
        </a>



    </div>


@endsection

@push('scripts')

    @endpush



