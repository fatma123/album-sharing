<?php
$home = $clients = $admins= $groups=$albums = FALSE;
if(isset(auth()->user()->Group->Permissions)){
foreach (auth()->user()->Group->Permissions as $pre) {
    if ($pre['name'] == "groups") {
        $groups = TRUE;
    }
    elseif ($pre['name'] == "home") {
        $home = TRUE;
    } elseif ($pre['name'] == "admins") {
        $admins = TRUE;
    }  elseif ($pre['name'] == "clients") {
        $clients = TRUE;
    }elseif ($pre['name'] == "albums") {
        $albums = TRUE;
    }
}
}
?>

<li>
    <a class="check_active" href="{{route('website.Album')}}">
        <i class="material-icons">home</i>
        <span>الموقع</span>
    </a>

</li>
@if($home)
    <li>
        <a class="check_active" href="{{route('admin.home')}}">
            <i class="material-icons">home</i>
            <span>الصفحه الرئيسه والاحصائيات</span>
        </a>

    </li>
@endif

@if($admins ||$clients)
<li class="header"> ادارة المستخدمين</li>
@endif
    @if($admins)
        <li>
            <a href="javascript:void(0);" class="menu-toggle">
                <i class="material-icons">assignment_ind</i>
                <span>أعضاء الإدارة</span>
            </a>
            <ul class="ml-menu">
                <li>
                    <a class="check_active" href="{{route('admin.admins.index')}}">

                        <span>  أعضاء الإدارة</span>
                    </a>
                </li>
            </ul>
        </li>
    @endif


    @if($clients)
        <li>
            <a href="javascript:void(0);" class="menu-toggle">
                <i class="material-icons">assignment_ind</i>
                <span>العملاء</span>
            </a>
            <ul class="ml-menu">
                <li>
                    <a class="check_active" href="{{route('admin.clients.index')}}">

                        <span>  جميع المستخدمين</span>
                    </a>
                </li>
            </ul>
        </li>

    @endif




@if($groups)
<li class="header"> إعدادات النظام والصلاحيات</li>
@endif
@if($groups)
    <li>
        <a href="javascript:void(0);" class="menu-toggle">
            <i class="material-icons">view_list</i>
            <span> المجموعات</span>
        </a>
        <ul class="ml-menu">
            <li>
                <a class="check_active" href="{{route('admin.groups.index')}}">
                    <span> عرض جميع المجموعات</span>
                </a>
            </li>
            <li>
                <a class="check_active" href="{{route('admin.groups.create')}}">
                    <span> اضافه مجموعة</span>
                </a>
            </li>
        </ul>
    </li>
@endif




