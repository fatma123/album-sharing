@include('admin.common.alert')
@include('admin.common.errors')


<div class="form-group form-float">
    <label class="form-label">المجموعة</label>
    <div class="form-line">
        {!! Form::select("group_id",groups(),null,['class'=>'form-control','placeholder'=>'اختر المجموعة'])!!}
    </div>
</div>


<div class="form-group form-float">
  <label class="form-label">الإسم</label>
  <div class="form-line">
    {!! Form::text("name",null,['class'=>'form-control','placeholder'=>'  الاسم  '])!!}
  </div>
</div>

<div class="form-group form-float">
  <label class="form-label">الإيميل</label>
  {!! Form::email("email",null,['class'=>'form-control','placeholder'=>'  الايميل '])!!}
  <div class="form-line">
  </div>
</div>


<div class="form-group form-float">
  <label class="form-label">كلمة المرور</label>
    {!! Form::input('password','password',null,['class'=>'form-control','placeholder'=>'كلمة المرور']) !!}
    <div class="form-line">
  </div>
</div>

<div class="form-group form-float">
  <label class="form-label">تكرار كلمة المرور</label>
    {!! Form::input('password','password_confirmation',null,['class'=>'form-control','placeholder'=>'تكرار كلمة المرور']) !!}

    <div class="form-line">
  </div>
</div>


{!! Form::input('hidden','role','admin',['class'=>'form-control']) !!}

@if(isset($item->id))
{!! Form::input('hidden','id',$item->id,['class'=>'form-control']) !!}
@endif
<button class="btn btn-primary waves-effect" type="submit">حفظ</button>
