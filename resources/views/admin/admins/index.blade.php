@extends('admin.layout.app')

@section('title')
    أعضاء إدارة النظام
@endsection
@section('header')
    @include('admin.datatable.headers')
@endsection
@section('content')
    @include('admin.common.alert')
    <!-- Exportable Table -->
    <div class="row clearfix">
        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
            <div class="card">
                <div class="header">
                    <h2>
                        أعضاء إدارة النظام
                    </h2>
                    <ul class="header-dropdown m-r--5">

                        <button class="btn btn-success" data-toggle="modal" data-target="#CreateModal">إضافة عضو جديد
                        </button>

                    </ul>
                </div>
                <div class="body">
                    <table class="table table-bordered table-striped table-hover dataTable js-exportable">
                        <thead>
                        <tr>
                            <th>#</th>
                            <th>الإسم</th>
                            <th>الايميل</th>
                            <th>عرض تفاصيل</th>
                            <th>العمليات</th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach($items as $key=>$item)
                            <tr>
                                <td>{{++$key}}</td>
                                <td>{{$item->name}}</td>
                                <td>{{$item->email}}</td>

                                <td>
                                    <button type="button" class="fa fa-eye" data-toggle="modal" data-target="#exampleModalCenter" onclick="Show({{$item->id}})">
                                    </button>

                                </td>

                                <td>

                                    <button class="btn btn-info btn-circle" data-toggle="modal" data-target="#EditModal">تعديل
                                    </button>


                                    {{--<a href="{{route('admin.admins.edit',['id'=>$item->id])}}"--}}
                                       {{--class="btn btn-info btn-circle"><i style="padding-top:5px;padding-left: 6px;"--}}
                                                                          {{--class="fa fa-pencil"data-toggle="modal" data-target="#EditModal"></i></a>--}}
                                    @if($item->email== "admin@admin.com")
                                    @else
                                        <a href="#" onclick="Delete({{$item->id}})" data-toggle="tooltip"
                                           data-original-title="حذف" class="btn btn-danger btn-circle"><i
                                                    style="padding-top: 5px;padding-left: 4px;"
                                                    class="fa fa-trash-o"></i></a>
                                    @endif
                                    {!!Form::open( ['route' => ['admin.admins.destroy',$item->id] ,'id'=>'delete-form'.$item->id, 'method' => 'Delete']) !!}
                                    {!!Form::close() !!}


                                </td>

                            </tr>
                        @endforeach

                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
    <!-- #END# Exportable Table -->




    <div class="modal fade" id="EditModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
         aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">اضافة عضو إدارة</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <form>
                        @csrf
                        <div class="form-group form-float">
                            <label class="form-label">المجموعة</label>
                            <div class="form-line">
                                {!! Form::select("group_id",groups(),null,['class'=>'form-control','placeholder'=>'اختر المجموعة','id'=>'group'])!!}
                            </div>
                        </div>


                        <div class="form-group form-float">
                            <label class="form-label">الإسم</label>
                            <div class="form-line">
                                {!! Form::text("name",null,['class'=>'form-control','placeholder'=>' الاسم  ','id'=>'name'])!!}
                            </div>
                        </div>

                        <div class="form-group form-float">
                            <label class="form-label">الإيميل</label>
                            {!! Form::email("email",null,['class'=>'form-control','placeholder'=>'  الايميل ',"id"=>"email"])!!}
                            <div class="form-line">
                            </div>
                        </div>


                        <div class="form-group form-float">
                            <label class="form-label">كلمة المرور</label>
                            {!! Form::input('password','password',null,['class'=>'form-control','placeholder'=>'كلمة المرور' , "id"=>"password"]) !!}
                            <div class="form-line">
                            </div>
                        </div>

                        <div class="form-group form-float">
                            <label class="form-label">تكرار كلمة المرور</label>
                            {!! Form::input('password','password_confirmation',null,['class'=>'form-control','placeholder'=>'تكرار كلمة المرور','id'=>"password_confirmation"]) !!}

                            <div class="form-line">
                            </div>
                        </div>


                        {!! Form::input('hidden','role','admin',['class'=>'form-control','id'=>'role']) !!}

                        @if(isset($item->id))
                            {!! Form::input('hidden','id',$item->id,['class'=>'form-control']) !!}
                        @endif
                        <div class="modal-footer">
                            <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                            <button type="submit"  id="createNewAdmin" class="btn btn-primary">Save changes</button>
                        </div>
                    </form>

                </div>

            </div>
        </div>
    </div>


    <!-- SHow modal -->

    <div class="modal fade" id="exampleModalCenter" tabindex="-1" role="dialog" aria-labelledby="exampleModalLongTitle" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLongTitle">تفاصيل الأدمن </h5>


                </div>

                <div class="modal-body">
                    <div class="form-group form-float">
                        <label class="form-label">المجموعة</label>
                        <div class="form-line">
                            {!! Form::text("group_id",isset($item->Group->name),['class'=>'form-control'])!!}
                        </div>
                    </div>


                    <div class="form-group form-float">
                        <label class="form-label">الإسم</label>
                        <div class="form-line">
                            {!! Form::text("name",$item->name,['class'=>'form-control','placeholder'=>' الاسم  ','id'=>'name'])!!}
                        </div>
                    </div>

                    <div class="form-group form-float">
                        <label class="form-label">الإيميل</label>
                        <input type="email" name="email" value="{{$item->email}}" class="form-control">
                        <div class="form-line">
                        </div>
                    </div>


                </div>

                <div class="modal-footer footer-button">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">غلق</button>
                </div>

            </div>
        </div>
    </div>



    <!-- Create Modal -->
    <div class="modal fade" id="CreateModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
    aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">

                    <h5 class="modal-title" id="exampleModalLabel">اضافة عضو إدارة</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <form>
                        @csrf
                        <div class="form-group form-float">
                            <label class="form-label">المجموعة</label>
                            <div class="form-line">
                                {!! Form::select("group_id",groups(),null,['class'=>'form-control','placeholder'=>'اختر المجموعة','id'=>'group'])!!}
                            </div>
                        </div>


                        <div class="form-group form-float">
                            <label class="form-label">الإسم</label>
                            <div class="form-line">
                                {!! Form::text("name",null,['class'=>'form-control','placeholder'=>' الاسم  ','id'=>'name'])!!}
                            </div>
                        </div>

                        <div class="form-group form-float">
                            <label class="form-label">الإيميل</label>
                            {!! Form::email("email",null,['class'=>'form-control','placeholder'=>'  الايميل ',"id"=>"email"])!!}
                            <div class="form-line">
                            </div>
                        </div>


                        <div class="form-group form-float">
                            <label class="form-label">كلمة المرور</label>
                            {!! Form::input('password','password',null,['class'=>'form-control','placeholder'=>'كلمة المرور' , "id"=>"password"]) !!}
                            <div class="form-line">
                            </div>
                        </div>

                        <div class="form-group form-float">
                            <label class="form-label">تكرار كلمة المرور</label>
                            {!! Form::input('password','password_confirmation',null,['class'=>'form-control','placeholder'=>'تكرار كلمة المرور','id'=>"password_confirmation"]) !!}

                            <div class="form-line">
                            </div>
                        </div>


                        {!! Form::input('hidden','role','admin',['class'=>'form-control','id'=>'role']) !!}

                        @if(isset($item->id))
                            {!! Form::input('hidden','id',$item->id,['class'=>'form-control']) !!}
                        @endif
                        <div class="modal-footer">
                            <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                            <button type="submit"  id="createNewAdmin" class="btn btn-primary">Save changes</button>
                        </div>
                    </form>

                </div>

            </div>
        </div>
    </div>

@endsection

@section('footer')
    @include('admin.datatable.scripts')
    <script>
        function Delete(id) {
            var item_id = id;
            console.log(item_id);
            swal({
                title: "هل أنت متأكد ",
                text: "هل تريد حذف هذا الأدمن ؟",
                icon: "warning",
                buttons: ["الغاء", "موافق"],
                dangerMode: true,

            }).then(function (isConfirm) {
                if (isConfirm) {
                    document.getElementById('delete-form' + item_id).submit();
                } else {
                    swal("تم االإلفاء", "حذف  الأدمن تم الغاؤه", 'info', {buttons: 'موافق'});
                }
            });
        }
    </script>
@endsection
