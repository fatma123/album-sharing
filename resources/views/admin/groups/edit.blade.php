@extends('admin.layout.app')

@section('title')
تعديل المجموعة
{{ $group->name }}
@endsection
@section('content')

<!-- Basic Validation -->
<div class="row clearfix">
  <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
    <div class="card">
      <div class="header">
        <h2>تعديل المجموعة       {{ $group->name }}</h2>
        <ul class="header-dropdown m-r--5">
            <a href="{{route('admin.groups.index')}}">   <button class="btn btn-danger">كل المجموعة</button></a>
         </ul>
      </div>
      <div class="body">
        {!!Form::model($group , ['route' => ['admin.groups.update' , $group->id] , 'method' => 'PATCH','files' => true]) !!}
        @include('admin.groups.form')
        {!!Form::close() !!}
      </div>
    </div>
  </div>
</div>


<!-- #END# Basic Validation -->
@endsection
