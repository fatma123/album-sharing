@include('admin.common.errors')
@include('admin.common.alert')


<div class="form-group form-float">
    <label class="form-label">اسم المجموعة</label>
    <div class="form-line">
        {!! Form::text("name",null,['class'=>'form-control','placeholder'=>'اكتب اسم المجموعة....'])!!}
    </div>
</div>

<hr>
<div class="form-group col-md-12 pull-left">
    <label>الصلاحيلات :</label>
</div>

@if(!isset($group))

    <div class="form-group col-md-12 pull-left">
        <label>الصفحة الرئيسية:</label>
    </div>
    <div class="checkbox checkbox-left checkbox-switchery col-xs-2 pull-right">
        <label style="margin-left: 20px;">
            الرئيسية
        </label>
        {{ Form::checkbox('permissions[]','home',false,['class'=>'switchery' ]) }}

    </div>
    <hr>
    <div class="form-group col-md-12 pull-left">
    <label>المعروض بالموقع:</label>
    </div>
    <div class="checkbox checkbox-left checkbox-switchery col-xs-2 pull-right">
        <label style="margin-left: 20px;">
            الألبومات
        </label>
        {{ Form::checkbox('permissions[]','albums',false,['class'=>'switchery' ]) }}

    </div>


    <div class="form-group col-md-12 pull-left">
        <label>ادارة مستخدمي النظام</label>
    </div>
    <div class="checkbox checkbox-left checkbox-switchery col-xs-2 pull-right">
        <label style="margin-left: 20px;">
            أعضاء الإدارة
        </label>
        {{ Form::checkbox('permissions[]','admins',false,['class'=>'switchery' ]) }}

    </div>


    <div class="checkbox checkbox-left checkbox-switchery col-xs-2 pull-right">
        <label style="margin-left: 20px;">
            العملاء
        </label>
        {{ Form::checkbox('permissions[]','clients',false,['class'=>'switchery' ]) }}
    </div>



    <div class="form-group col-md-12 pull-left">
        <label>صلاحيات المستخدمين:</label>
    </div>

    <div class="checkbox checkbox-left checkbox-switchery col-xs-2 pull-right">
        <label style="margin-left: 20px;">
            المجموعات
        </label>
        {{ Form::checkbox('permissions[]','groups',false,['class'=>'switchery' ]) }}

    </div>




@else
    <?php
    $home = $clients = $admins= $groups=$albums = FALSE;
    foreach (auth()->user()->Group->Permissions as $pre) {
        if ($pre['name'] == "groups") {
            $groups = TRUE;
        }
        elseif ($pre['name'] == "home") {
            $home = TRUE;
        } elseif ($pre['name'] == "admins") {
            $admins = TRUE;
        }  elseif ($pre['name'] == "clients") {
            $clients = TRUE;
        }elseif ($pre['name'] == "albums") {
            $albums = TRUE;
        }
    }
    ?>



    <div class="form-group col-md-12 pull-left">
        <label>الصفحة الرئيسية:</label>
    </div>
    <div class="checkbox checkbox-left checkbox-switchery col-xs-2 pull-right">
        <label style="margin-left: 20px;">
            الرئيسية
        </label>
        {{ Form::checkbox('permissions[]','home',$home,['class'=>'switchery' ]) }}

    </div>
    <hr>



    <div class="form-group col-md-12 pull-left">
        <label>ادارة مستخدمي النظام</label>
    </div>
    <div class="checkbox checkbox-left checkbox-switchery col-xs-2 pull-right">
        <label style="margin-left: 20px;">
            أعضاء الإدارة
        </label>
        {{ Form::checkbox('permissions[]','admins',$admins,['class'=>'switchery' ]) }}

        <hr>


    </div>



    <div class="checkbox checkbox-left checkbox-switchery col-xs-2 pull-right">
        <label style="margin-left: 20px;">
            العملاء
        </label>
        {{ Form::checkbox('permissions[]','clients',$clients,['class'=>'switchery' ]) }}

    </div>

    <div class="checkbox checkbox-left checkbox-switchery col-xs-2 pull-right">
        <label style="margin-left: 20px;">
            الالبومات
        </label>
        {{ Form::checkbox('permissions[]','albums',$albums,['class'=>'switchery' ]) }}

    </div>

    <div class="form-group col-md-12 pull-left">
        <label>صلاحيات المستخدمين:</label>
    </div>

    <div class="checkbox checkbox-left checkbox-switchery col-xs-2 pull-right">
        <label style="margin-left: 20px;">
            المجموعات
        </label>
        {{ Form::checkbox('permissions[]','groups',$groups,['class'=>'switchery' ]) }}

    </div>




@endif



<div class="text-center col-md-12">
    <div class="text-right">
        <button type="submit" class="btn btn-success">حفظ <i class="icon-arrow-left13 position-right"></i></button>
    </div>
</div>


@push('scripts')

    {{--<script>--}}


        {{--$val = $('#OperationType option:selected').val();--}}

        {{--if ($val === 'deduction') {--}}
            {{--$(".deduction_type").show();--}}
            {{--$(".deduction_date").show();--}}
            {{--$(".deduction_status").show();--}}
            {{--$('#deduction_type').prop('disabled', false);--}}
            {{--$('#deduction_date').prop('disabled', false);--}}
            {{--$('#deduction_status').prop('disabled', false);--}}
        {{--} else {--}}

            {{--$('#deduction_type').prop('disabled', true);--}}
            {{--$('#deduction_date').prop('disabled', true);--}}
            {{--$('#deduction_status').prop('disabled', true);--}}

            {{--$(".deduction_type").hide();--}}
            {{--$(".deduction_date").hide();--}}
            {{--$(".deduction_status").hide();--}}

        {{--}--}}
        {{--$('#OperationType').on('change', function () {--}}
            {{--if (this.value == 'deduction' || this.value == 'ancestor') {--}}
                {{--$('#deduction_type').prop('disabled', false);--}}
                {{--$('#deduction_date').prop('disabled', false);--}}
                {{--$('#deduction_status').prop('disabled', false);--}}
                {{--$(".deduction_type").show();--}}
                {{--$(".deduction_date").show();--}}
                {{--$(".deduction_status").show();--}}

            {{--} else {--}}
                {{--$('#deduction_type').prop('disabled', true);--}}
                {{--$('#deduction_date').prop('disabled', true);--}}
                {{--$('#deduction_status').prop('disabled', true);--}}
                {{--$(".deduction_type").hide();--}}
                {{--$(".deduction_date").hide();--}}
                {{--$(".deduction_status").hide();--}}

            {{--}--}}
        {{--});--}}
    {{--</script>--}}
<script>
    $( ".switchery" ).click(function() {

        if(this.checked){
            $( "."+this.id).removeAttr("disabled");
            $( "."+this.id).not(this).prop('checked', this.checked);

        } else {
            $( "."+this.id).attr("disabled", true);
            $( "."+this.id).removeAttr('checked');
        }

    });
</script>

@endpush

