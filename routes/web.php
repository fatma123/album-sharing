<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


Auth::routes();

//******************************* DASHBOARD ROUTES  ***********************************

//DASHBOARD LOGIN

Route::group(['prefix' => 'dashboard-login', 'as' => 'login.', 'namespace' => 'Admin'], function () {
    Route::get('/', 'LoginController@showLogin')->name('login');
    Route::post('/', 'LoginController@Login');
});


Route::group(['prefix' => 'dashboard', 'as' => 'admin.', 'namespace' => 'Admin', 'middleware' => 'admin'], function () {


    //Home 
    Route::get('/', 'IndexController@index')->name('home')->middleware('role:home');

    //USERS and Users Operations
    Route::resource('admins', 'AdminController')->middleware('role:admins');
    Route::get('admins/delete/{id}', 'UsersController@AdminDelete')->middleware('role:admins');


    Route::resource('clients', 'UsersController')->middleware('role:clients');
    Route::get('clients/delete/{id}', 'UsersController@UserDelete')->middleware('role:clients');

    //Albums
    Route::resource('albums', 'UserAlbumsController')->middleware('role:albums');


    //GROUPS AND PERMISSIONS
    Route::resource('groups', 'GroupsController')->middleware('role:groups');

});


//******************************* WEBSITE ROUTES  ***********************************

Route::group(['prefix' => '/', 'as' => 'website.', 'namespace' => 'Website'], function () {

    Route::get('/', 'IndexController@Albums')->name('Album');

    //PROFILE
    Route::get('profile', 'IndexController@Profile')->name('profile');
    Route::get('profile-edit', 'IndexController@EditProfile')->name('EditProfile');
    Route::post('update', 'IndexController@UpdateProfile')->name('UpdateProfile');

    //AJaxRoutes
    Route::get('album/delete/{id}', 'AjaxOperationsController@UserAlbumDelete');


});
