$('#createNewAdmin').click(function () {
    var name = $("#name").val();
    var email = $('#email').val();
    var password = $("#password").val();
    var password_confirmation = $("#password_confirmation").val();
    var role = $("#role").val();
    var group_id = $("#group").val();

    if (name != '' && email != '' && password != '' && password_confirmation != '' && role != '' && group_id != '') {
        e.preventDefault();
        $.post('dashboard/admins/create', {
            _token:"{{csrf_token()}}",
            name: name,
            email: email,
            password: password,
            password_confirmation: password_confirmation,
            role: role
        }, function(data) {
            if (data.response == 'Admin saved successfully.'){
                window.location.href = 'dashboard/index';
            } else{
                alert('Error occured');
            }
        });
    }
});